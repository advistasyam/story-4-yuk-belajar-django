from django.urls import path
from . import views

app_name = 'matakuliah'

urlpatterns = [
    path('', views.create_view, name='create_view'),
    path('hasil/', views.list_view, name='list_view'),
    path('hasil/<int:id>/', views.nama_matkul, name='nama_matkul'),
    path('hasil/<int:id>/delete/', views.hapus_matkul, name='hapus_matkul'),
]