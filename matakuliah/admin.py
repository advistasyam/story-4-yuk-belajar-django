from django.contrib import admin
from .models import DataMatkul, Tugas

# Register your models here.
admin.site.register(DataMatkul)
admin.site.register(Tugas)