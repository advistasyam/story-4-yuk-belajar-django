from django.db import models

# Create your models here.
class DataMatkul(models.Model):
    Nama_Mata_Kuliah = models.CharField(max_length=120)
    Dosen_Pengajar = models.CharField(max_length=120)
    Deskripsi = models.TextField(null=True)
    Semester = models.CharField(max_length=120)
    Ruang = models.CharField(max_length=120)
    Jumlah_SKS = models.IntegerField()

    def __str__(self):
        return self.Nama_Mata_Kuliah

class Tugas(models.Model):
    Nama_Tugas = models.CharField(max_length=120)
    Deadline = models.DateField()
    matakuliah = models.ForeignKey(DataMatkul, on_delete=models.CASCADE)

    class Meta:
        ordering = ['Deadline']

    def __str__(self):
        return self.Nama_Tugas