from django import forms

from .models import DataMatkul

class DataForm(forms.ModelForm):
    class Meta:
        model = DataMatkul
        fields = [
            'Nama_Mata_Kuliah',
            'Dosen_Pengajar',
            'Deskripsi',
            'Semester',
            'Ruang',
            'Jumlah_SKS',
        ]