from django.shortcuts import render, get_object_or_404, redirect
from .models import DataMatkul, Tugas
from .forms import DataForm

# Create your views here.

def create_view(request):
    form = DataForm(request.POST or None)
    if form.is_valid():
        form.save()
        form = DataForm()
    
    context = {
        'form' : form
    }
    return render(request, "landing.html", context)

def list_view(request):
    queryset = DataMatkul.objects.all() #membuat list object

    context = {
        'object_list' : queryset
    }
    return render(request, "list_matkul.html", context)

def hapus_matkul(request, id):
    obj_hapus = get_object_or_404(DataMatkul, id=id) #mengambil nama object

    if request.method == 'POST':
        obj_hapus.delete()
        return redirect('../../')

    context = {
        'object_hapus' : obj_hapus
    }
    return render(request, "hapus_matkul.html", context)

def nama_matkul(request, id):
    obj = get_object_or_404(DataMatkul, id=id)
    obj_tugas = Tugas.objects.all()

    context = {
        'object' : obj,
        'tugas'  : obj_tugas,
    }
    return render(request, "nama_matkul.html", context)

