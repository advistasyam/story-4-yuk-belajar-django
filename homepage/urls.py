from django.urls import path
from . import views

app_name = 'homepage'

urlpatterns = [
    path('', views.home, name='home'),
    path('comingsoon/', views.home2, name='home2'),
    path('story1/', views.story1, name='story1'),
    path('template/', views.contoh_navigasi_bersama, name='contoh_navigasi_bersama'),
    path('extends/', views.contoh_menggunakan_template, name='contoh_menggunakan_template'),
    path('include/', views.contoh_method_include, name='contoh_method_include'),
]