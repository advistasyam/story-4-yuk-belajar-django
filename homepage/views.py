from django.shortcuts import render

# Create your views here.
def home(request):
    return render(request, 'home.html')

def home2(request):
    return render(request, 'home2.html')

def story1(request):
    return render(request, 'story1.html')

def contoh_navigasi_bersama(request):
    return render(request, 'contoh_navigasi_bersama.html')

def contoh_menggunakan_template(request):
    return render(request, 'contoh_menggunakan_template.html')

def contoh_method_include(request):
    return render(request, 'contoh_method_include.html')