from django.urls import path
from . import views

app_name = 'story6'

urlpatterns = [
    path('', views.landing, name='landing'),
    path('hasil/', views.listAcara, name='listAcara'),
    path('hasil/register/<int:index>/', views.register, name='register'),
    path('hasil/<int:index>/', views.hapus_nama, name='hapus_nama'),
]